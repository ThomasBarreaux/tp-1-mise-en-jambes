# I. Exploration locale en solo
## 1. Affichage d'informations sur la pile TCP/IP locale

nom, adresse MAC et adresse IP de l'interface WiFi : 
ipconfig /all
```
Carte réseau sans fil Wi-Fi :
   Adresse IPv4. . . . . . . . . . . . . .: 10.33.19.127
   Adresse physique . . . . . . . . . . . : 44-AF-28-E3-3B-B9
```
Affichez votre gateway
ipconfig /all
```
    Passerelle par défaut. . . . . . . . . : 10.33.19.254
```

Trouvez comment afficher les informations sur une carte IP (change selon l'OS)
paramètre -> réseau et internet -> modifier les options d'adaptateurs -> cliquer sur carte WIFI -> status -> Détail
Suffixe DNS propre à la connexion: 
```
Adresse physique: ‎44-AF-28-E3-3B-B9
DHCP activé: Oui
Adresse IPv4: 10.33.19.127
Passerelle par défaut IPv4: 10.33.19.254
```

Questions
à quoi sert la gateway dans le réseau d'YNOV ?
La Gateway d'Ynov permet de se connecter aux autres réseaux internet.



## 2. Modifications des informations
### A. Modification d'adresse IP (part 1)
paramètre -> réseau et internet -> modifier les options d'adaptateurs -> cliquer sur carte WIFI -> status -> Propriété -> Utiliser l'adresse ip suivante -> Changer le dernier octet et mettre la meme passerelle et masque de sous réseau

Perte d'internet car l'adresse est déjà assigné à quelqu'un.

# II. Exploration locale en duo
## 1. Modification d'adresse IP


Afficher et consulter la table arp
```
arp -a

Interface : 192.168.5.6 --- 0x4
  192.168.5.5           e4-b9-7a-e9-da-17     dynamique   (ip de la 2ème machine connectée en ethernet)
```

Pour la partie avec les 2 pc reliés en ethernet j'ai pas pensé à prendre des screenshots et comme du coup je n'ai pas de preuve mais je l'ai fais avec Valentin et on a réussi

Ping :
```
Envoi d’une requête 'Ping'  1.1.1.1 avec 32 octets de données :
Réponse de 1.1.1.1 : octets=32 temps<1ms TTL=64
```
Tracert : 
```
tracert -4 192.168.137.5 (ip de la carte réseau de la 2ème machine)

Détermination de l’itinéraire vers Asus-Valentin [192.168.137.5]
avec un maximum de 30 sauts :

  1     1 ms     1 ms     1 ms  ASUS-VALENTIN [192.168.137.5]
```
  

Port netcat : Je n'ai pas pensé à prendre de screenshots non plus mais j'ai copié collé le résultat du CMD (fais avec Valentin)
```
nc.exe -l -p 8888
h
a
```
# III. Manipulations d'autres outils/protocoles côté client

1. DHCP
Bon ok vous savez définir des IPs à la main. Mais pour être dans le réseau YNOV, vous l'avez jamais fait.
C'est le serveur DHCP d'YNOV qui vous a donné une IP.
Une fois que le serveur DHCP vous a donné une IP, vous enregistrer un fichier appelé bail DHCP qui contient, entre autres :

l'IP qu'on vous a donné
le réseau dans lequel cette IP est valable

🌞Exploration du DHCP, depuis votre PC

afficher l'adresse IP du serveur DHCP du réseau WiFi YNOV
PS C:\Users\spakg> ipconfig /all


```
Serveur DHCP . . . . . . . . . . . . . : 10.33.19.254
```


cette adresse a une durée de vie limitée. C'est le principe du bail DHCP (ou DHCP lease). Trouver la date d'expiration de votre bail DHCP

```
   Bail obtenu. . . . . . . . . . . . . . : mercredi 28 septembre 2022 08:26:22
   Bail expirant. . . . . . . . . . . . . : jeudi 29 septembre 2022 08:26:21
```

1. DNS
Le protocole DNS permet la résolution de noms de domaine vers des adresses IP. Ce protocole permet d'aller sur google.com plutôt que de devoir connaître et utiliser l'adresse IP du serveur de Google.
Un serveur DNS est un serveur à qui l'on peut poser des questions (= effectuer des requêtes) sur un nom de domaine comme google.com, afin d'obtenir les adresses IP liées au nom de domaine.
Si votre navigateur fonctionne "normalement" (il vous permet d'aller sur google.com par exemple) alors votre ordinateur connaît forcément l'adresse d'un serveur DNS. Et quand vous naviguez sur internet, il effectue toutes les requêtes DNS à votre place, de façon automatique.


🌞 trouver l'adresse IP du serveur DNS que connaît votre ordinateur
PS C:\Users\spakg> ipconfig /all


```
Serveurs DNS. . .  . . . . . . . . . . : 8.8.8.8
```



🌞 utiliser, en ligne de commande l'outil nslookup (Windows, MacOS) ou dig (GNU/Linux, MacOS) pour faire des requêtes DNS à la main


faites un lookup (lookup = "dis moi à quelle IP se trouve tel nom de domaine")

pour google.com

```
PS C:\Users\spakg> nslookup google.com
bash Addresses:  2a00:1450:4007:808::200e 216.58.215.46 
```
pour ynov.com

```
PS C:\Users\spakg> nslookup ynov.com
bash Addresses:  2606:4700:20::ac43:4ae2 2606:4700:20::681a:ae9 2606:4700:20::681a:be9 104.26.10.233 104.26.11.233 172.67.74.226 
```
interpréter les résultats de ces commandes

Un nom de domaine peut avoir plusieurs adresses IP


déterminer l'adresse IP du serveur à qui vous venez d'effectuer ces requêtes


faites un reverse lookup (= "dis moi si tu connais un nom de domaine pour telle IP")

pour l'adresse 78.74.21.21

```
PS C:\Users\spakg> nslookup 78.74.21.21
bash Nom :    host-78-74-21-21.homerun.telia.com 
```
pour l'adresse 92.146.54.88

```
PS C:\Users\spakg> nslookup 92.146.54.88
bash *** dns.google ne parvient pas à trouver 92.146.54.88 : Non-existent domain 
```
interpréter les résultats

Quand on lui demande de chercher grâce à une adresse IP il nous renvoie un nom de domaine, mais si elle n'en possede pas alors un message s'affiche pour nous signaler qu'il n'y en a pas



Wiresharck
Partie netcat
```
17	7.796578	192.168.137.2	192.168.137.5	TCP	56	8888 → 1241 [PSH, ACK] Seq=1 Ack=1 Win=1026 Len=2
```
Partie ping
```
5	0.908162	192.168.137.5	192.168.137.2	ICMP	74	Echo (ping) request  id=0x0001, seq=60/15360, ttl=128 (reply in 6)
```

Requete DNS google
```
484	9.921963	8.8.8.8	10.33.19.96	DNS	121	Standard query response 0x0987 A cloud.bluestacks.com CNAME ghs.google.com A 216.58.214.179
```


Barreaux Thomas B2 B